//Ashley Vu -2034284
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

public String getManufacturer(){
    return this.manufacturer;
}
public int getNumGears(){
    return this.numberGears;
}
public double getSpeed(){
    return this.maxSpeed;
}
public Bicycle(String manufacturer, int numberGears, double maxSpeed){
    this.manufacturer = manufacturer;
    this.numberGears = numberGears;
    this.maxSpeed= maxSpeed;
}
public String toString(){
    String s = "";
    s+= "Manufacturer: "+ this.manufacturer + "; Number of Gears: " + this.numberGears + "; MaxSpeed: " + this.maxSpeed;
    return s;
}
}
